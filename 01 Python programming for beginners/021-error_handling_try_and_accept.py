#!/usr/bin/env python
#title           :021-error_handling_try_and_accept.py
#description     :Error Handling - Try and Accept.
#author          :masedos@gmail.com
#date            :01-06-2016
#version         :0.1
#usage           :python 021-error_handling_try_and_accept.py
#notes           :
#python_version  :3.5.1

try:
    print(x)
    
except TypeError as t: 
    print('TypeError triggered')
    
except NameError as n: 
    print('NameError triggered')
    print(str(n))
    
except Exception as e: 
    print('General exception')
    print(str(e))