#!/usr/bin/env python
#title           :013-common_python_errors.py
#description     :Common python erros.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 013-common_python_errors.py
#notes           :
#python_version  :3.5.1

###NameError: name 'varaible' is not defined
# vstisblr = 55
# print(varaible)


###IndentationError: expected an indented block
# def func1():

# def func2():
#     print(2)

###IndentationError: unexpected indent
# def task():
#     print('1')
# print('2')
#     print('3')

###SyntaxError: unexpected EOF while parsing
# print('Hey there, how are you?'