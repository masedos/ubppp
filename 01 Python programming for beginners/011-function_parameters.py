#!/usr/bin/env python
#title           :011-function_parameters.py
#description     :Functions parameters.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 011-function_parameters.py
#notes           :
#python_version  :3.5.1


def website(font, background_color, font_size, font_color):
    print('Font:', font)
    print('Background color:', background_color)
    print('fonte size:', font_size)
    print('Font color:', font_color)
website('Ariel', 'Grey', '11', 'Blue')