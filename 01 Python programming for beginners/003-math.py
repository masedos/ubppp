#!/usr/bin/env python
#title           :003-math.py
#description     :Study basic math.
#author          :masedos@gmail.com
#date            :19-05-2016
#version         :0.1
#usage           :python 003-math.py
#notes           :
#python_version  :3.5.1

print(1+3)

print(1-3)

print(1*3)

print(1/3)

print(1.5/3.6)

print(4**2)