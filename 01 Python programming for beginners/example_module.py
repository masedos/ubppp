#!/usr/bin/env python
#title           :example_module.py
#description     :Examplo module.
#author          :masedos@gmail.com
#date            :30-05-2016
#version         :0.1
#usage           :python examplo_module.py
#notes           :
#python_version  :3.5.1

def example_func(data):
    print(data)