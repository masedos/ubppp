#!/usr/bin/env python
#title           :009-if_elif_else_statements.py
#description     :If, elif and else statements.
#author          :masedos@gmail.com
#date            :27-05-2016
#version         :0.1
#usage           :python 009-if_elif_else_statements.py
#notes           :
#python_version  :3.5.1

x = 3
y = 7
z = 10

if x < y and x < z:
    print('something here was the case')
elif x < z:
    print(x, 'is less than', z)
elif y < z:
    print(y, 'is less than', z)
else:
    print('nothing was the case')
