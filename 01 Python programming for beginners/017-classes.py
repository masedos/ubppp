#!/usr/bin/env python
#title           :017-classes.py
#description     :Classes.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 017-classes.py
#notes           :
#python_version  :3.5.1

class Calc:
	def add(x, y):
		answer = x + y
		print(answer)

	def sub(x, y):
		answer = x - y
		print(answer)

	def mult(x, y):
		answer = x * y
		print(answer)

	def div(x, y):
		answer = x / y
		print(answer)