#!/usr/bin/env python
#title           :002-strings.py
#description     :Print and strings.
#author          :masedos@gmail.com
#date            :19-05-2016
#version         :0.1
#usage           :python 002-stringse.py
#notes           :
#python_version  :3.5.1

print("Double quotes")
print('concatena' + 'tion')
print('Hello', 'there')
print('I am number', 4)

