#!/usr/bin/env python
#title           :020-making_modules.py
#description     :Making modules.
#author          :masedos@gmail.com
#date            :30-05-2016
#version         :0.1
#usage           :python 020-making_modules.py
#notes           :
#python_version  :3.5.1

import example_module

example_module.example_func('This a example module from file example_module.py')