#!/usr/bin/env python
#title           :016-reading_from_a_file.py
#description     :Reading from a file.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 016-reading_from_a_file.py
#notes           :
#python_version  :3.5.1

print('#'*40, 'READ ME')

read_me = open('example_file.txt', 'r').read()
print(read_me)

print('#'*40, 'SPLIT')

split_me = read_me.split('\n')
print(split_me)

print('#'*40, 'READ LINES')

read_lines = open('example_file.txt', 'r').readlines()
print(read_lines)

print('#'*40, 'FINISH')
