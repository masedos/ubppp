#!/usr/bin/env python
#title           :012-global_and_local_variables.py
#description     :Global and local variables.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 012-global_and_local_variables.py
#notes           :
#python_version  :3.5.1

x = 6

def example():
    z = 6
    print(z)
    print(x)
    
    z += 1
    print(z)
    
    y = x + 1
    print(y)
    return y
    
x = example()
print(x)