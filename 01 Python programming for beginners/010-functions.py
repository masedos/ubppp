#!/usr/bin/env python
#title           :010-functions.py
#description     :Functions.
#author          :masedos@gmail.com
#date            :27-05-2016
#version         :0.1
#usage           :python 010-functions.py
#notes           :
#python_version  :3.5.1

def example():
    x = 1   
    y = 3
    print(x+y)
    
    if x < y:
        print(x, 'is less than',y)
    
example()
