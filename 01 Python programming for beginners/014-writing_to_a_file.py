#!/usr/bin/env python
#title           :014-writing_to_a_file.py
#description     :Writing to a file.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 014-writing_to_a_file.py
#notes           :
#python_version  :3.5.1

write_me = 'example of a text'
save_file = open('example_file.txt', 'w')
save_file.write(write_me)
save_file.close()