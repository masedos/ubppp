#!/usr/bin/env python
#title           :001-hello.py
#description     :Print Hello World.
#author          :masedos@gmail.com
#date            :19-05-2016
#version         :0.1
#usage           :python 001-hello.py
#notes           :
#python_version  :3.5.1

print('Hello World')

