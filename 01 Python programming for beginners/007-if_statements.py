#!/usr/bin/env python
#title           :007-if_statements.py
#description     :If statements.
#author          :masedos@gmail.com
#date            :27-05-2016
#version         :0.1
#usage           :python 007-if_statements.py
#notes           :
#python_version  :3.5.1


x = 2
y = 7
z = 10

if x > y:
    print(x,'is greater than', y)
    
if x < y:
    print(x,'is less than', y)
    
if x == y:
    print(x,'is the same as', y)
    
if x == '2':
    print(x,'equals to string 2')
 
# cannot do thiss   
# if x  < '2':
#     print(x,'is less than 2')

if x  <= 2:
    print(x,'is less than 2')

if x <= y:
    print(x,'is less tha or equal to', y)
    
if z > y > x:
    print(z,'is greater than', y, 'which is greater than', x)