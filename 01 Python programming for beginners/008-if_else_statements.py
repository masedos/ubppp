#!/usr/bin/env python
#title           :008-if_else_statements.py
#description     :If else statements.
#author          :masedos@gmail.com
#date            :27-05-2016
#version         :0.1
#usage           :python 008-if_else_statements.py
#notes           :
#python_version  :3.5.1

x = 13
y = 6

if x < y:
    print(x, 'is less than', y)
    
if x > y:
    print(x, 'is greater than' ,y)

else:
    print(x, 'is not less than' ,y)


