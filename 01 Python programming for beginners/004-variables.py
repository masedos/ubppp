#!/usr/bin/env python
#title           :004-variables.py
#description     :Study variables.
#author          :masedos@gmail.com
#date            :19-05-2016
#version         :0.1
#usage           :python 004-variables.py
#notes           :
#python_version  :3.5.1

ex_var = 100
print(ex_var)

op_var =  ex_var / 5.3
print(op_var)

_100ma = 5
print(_100ma)