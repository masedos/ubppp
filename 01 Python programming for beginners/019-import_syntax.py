#!/usr/bin/env python
#title           :018-input_and_statistics.py
#description     :Inout and statistics.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 018-input_and_statistics.py
#notes           :
#python_version  :3.5.1

# name = input('What is your name? >  ')
# print('Hello', name)

import statistics

ex_list = [5, 3, 2, 5, 9, 1, 0, 7, 6, 8]

x = statistics.mean(ex_list)
print(x)

x = statistics.median(ex_list)
print(x)

x = statistics.mode(ex_list)
print(x)

x = statistics.stdev(ex_list)
print(x)

x = statistics.variance(ex_list)
print(x)