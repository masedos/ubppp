#!/usr/bin/env python
#title           :024-project_making_a_python_program.py
#description     :Project - Making a Python Program.
#author          :masedos@gmail.com
#date            :02-06-2016
#version         :0.1
#usage           :python 024-project_making_a_python_program.py
#notes           :
#python_version  :3.5.1

import statistics
import sys

admins = {'python':'pass123@', 'user':'pass'}

student_dict = {'Jeff':[78,88,93], 
                'Alex':[92,76,88],
                'Sam':[89,92,93]}

def enter_grades():
    name_to_enter = input('Student name > ')
    grade_to_enter = float(input('Grade > '))
    
    if name_to_enter in student_dict:
        print('Adding grade...')
        student_dict[name_to_enter] .append(float(grade_to_enter))
    else:
        print('Student does not exist')
        
    print(student_dict)

def remove_student():
    name_to_remove = input('What student to remove? >')
    if name_to_remove in student_dict:
        print('Removing student...')
        del student_dict[name_to_remove]
        print(student_dict)


def student_avgs():
    for each_student in student_dict:
        grade_list = student_dict[each_student]
        avg_grade = statistics.mean(grade_list)
        print(each_student, 'has an average grade of: ', avg_grade)
    else:
        print('Not found')

def main():
    print("""
    Welcome to the Grade Central
    
    [1] - Enter Grades
    [2] - Remove Student
    [3] - Student Average Grades
    [4] - Exit
    """)
    
    action = input('What would you like to do today? <Enter a number> ')
    
    if action == '1':
        enter_grades()
    elif action == '2':
        remove_student()
    elif action == '3':
        student_avgs()
    elif action == '4':
        sys.exit(0)
        print('Out of the system')
    else:
        print('No valid choice was given, try again')

login = input('Username: ')
passwd = input('Password: ')

if login in admins:
    if admins[login] == passwd:
        print('Welcome, ', login)
        while True:
            main()
        else:
            print('Invalid password or login')

else:
    print('Access denied!')