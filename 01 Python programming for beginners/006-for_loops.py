#!/usr/bin/env python
#title           :006-for_loops.py
#description     :For loops.
#author          :masedos@gmail.com
#date            :27-05-2016
#version         :0.1
#usage           :python 006-for_loops.py
#notes           :
#python_version  :3.5.1

example_list = [1, 5, 6, 4, 9, 12, 0, 13]

for thing in example_list:
    print(thing)

print('*'*21)

for x in range(1, 11):
    print(x)