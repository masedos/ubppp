#!/usr/bin/env python
#title           :022-lists_vs_tuples_and_list_manipulation.py
#description     :Lists vs Tuples and List Manipulation.
#author          :masedos@gmail.com
#date            :01-06-2016
#version         :0.1
#usage           :python 022-lists_vs_tuples_and_list_manipulation.py
#notes           :
#python_version  :3.5.1

def example():
    return 15, 19
    
a,b = example()

print(a)
print(b)

x = [1, 3, 4, 5, 9, 2, 0, 3, 6, 7, 11, 7]
print(x)
print(len(x),'elements')

print('\n')

x.append(12)
print(x)
print(sorted(x))
print(len(x),'elements')

print('\n')

x.insert(5,7)
print(x)
print(sorted(x))
print(len(x),'elements')

print('\n')

x.remove(7)
print(x)
print(sorted(x))
print(len(x),'elements')

print('\n')

print('found: ',x.index(12))
print(sorted(x))
print(len(x),'elements')

print('\n')

print('count: ',x.count(7))
print(x)
x.sort()
print(x)
print(len(x),'elements')

print('\n')

x = ['Spot', 'Zack', 'Dave', 'Jan', 'Cam']
print(x)
x.sort()
print(x)
print(len(x),'elements')

print('\n')

print(x)
x.reverse()
print(x)
print(len(x),'elements')
print(type(x))