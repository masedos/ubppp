#!/usr/bin/env python
#title           :023-dictionaries.py
#description     :Dictionaries.
#author          :masedos@gmail.com
#date            :01-06-2016
#version         :0.1
#usage           :python 023-dictionaries.py
#notes           :
#python_version  :3.5.1

grade_dict = {'Kelly':89, 'David':65, 'Jack':95, 'Samantha':78}
print(grade_dict)

print(grade_dict['David'])

grade_dict['David'] = 56
print(grade_dict)

grade_dict['Jessy'] = 92
print(grade_dict)

del grade_dict['David']
print(grade_dict)

grade_dict = {'Kelly':[89, 89], 'Jessy':[92,99], 'Jack':[95,87], 'Samantha':[78,89]}
print(grade_dict)
print(type(grade_dict))

print(grade_dict['Jessy'])
print(grade_dict['Jessy'][0])