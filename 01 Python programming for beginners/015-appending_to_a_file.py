#!/usr/bin/env python
#title           :015-appending_to_a_file.py
#description     :Appending to a file.
#author          :masedos@gmail.com
#date            :29-05-2016
#version         :0.1
#usage           :python 015-appending_to_a_file.py
#notes           :
#python_version  :3.5.1

append_me = 'example of appending text'
save_file = open('example_file.txt', 'a')
save_file.write('\n')
save_file.write(append_me)
save_file.close()