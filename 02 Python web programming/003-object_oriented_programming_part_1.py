#!/usr/bin/env python
#title           :003-object_oriented_programming_part_1.py
#description     :Object Oriented Programming Part 1.
#author          :masedos@gmail.com
#date            :10-06-2016
#version         :0.1
#usage           :python 003-object_oriented_programming_part_1.py
#notes           :
#python_version  :3.5.1

class Program():
    
    def __init__(self, *args, **kwargs):
        self.lang = input('What language?: ')
        self.version = float(input('Version?: '))
        self.skill = input('What skill level?: ')
        
p1 = Program()

print(p1.lang)
print(p1.version)
print(p1.skill)  