#!/usr/bin/env python
#title           :004-object_oriented_programming_part_2.py
#description     :Object Oriented Programming Part 2.
#author          :masedos@gmail.com
#date            :23-06-2016
#version         :0.1
#usage           :python 004-object_oriented_programming_part_2.py
#notes           :
#python_version  :3.5.1

class Program():
    
    def __init__(self, *args, **kwargs):
        self.lang = input('What language?: ')
        self.version = float(input('Version?: '))
        self.skill = input('What skill level?: ')
        
        
        def upgrade(self):
            new_version = float(input('What version?: '))
            print('We have updated the version for', self.lang)
            self.version = new_version
            
        
p1 = Program()

